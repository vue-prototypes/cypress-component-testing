import Stepper from "./Stepper.vue";

describe("<Stepper />", () => {
  it("renders", () => {
    // see: https://on.cypress.io/mounting-vue
    cy.mount(Stepper);
  });

  it("stepper should default to 0", () => {
    cy.mount(Stepper);
    cy.get("[data-cy=counter]").should("have.text", "0");
  });

  it('supports an "initial" prop to set the value', () => {
    cy.mount(Stepper, { props: { initial: 100 } });
    cy.get("[data-cy=counter").should("have.text", "100");
  });

  it("increments counter when the increment button is pressed", () => {
    cy.mount(Stepper);
    cy.get("[data-cy=increment]").click();
    cy.get("[data-cy=counter").should("have.text", "1");
  });

  it("decrements counter when the decrement button is pressed", () => {
    cy.mount(Stepper);
    cy.get("[data-cy=decrement]").click();
    cy.get("[data-cy=counter").should("have.text", "-1");
  });

  it("emits change event with the new value upon clicking the + button", () => {
    const onChangeSpy = cy.spy().as("onChangeSpy");
    cy.mount(Stepper, { props: { onChange: onChangeSpy } });
    cy.get("[data-cy=increment]").click();
    cy.get("@onChangeSpy").should("have.been.calledWith", 1);
  });

  it("emits change event with the new value upon clicking the - button", () => {
    const onChangeSpy = cy.spy().as("onChangeSpy");
    cy.mount(Stepper, { props: { onChange: onChangeSpy } });
    cy.get("[data-cy=decrement]").click();
    cy.get("@onChangeSpy").should("have.been.calledWith", -1);
  });
});
